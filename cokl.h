#ifndef COKL_H
#define COKL_H

enum node_type {
	t_id,
	t_integer, t_float, t_bool,
	t_arithm, t_if, t_while, t_print, t_declaration, t_block,
	t_expr, t_value,
	t_undef
};

/* -- */

struct id_node {
	char *name;
};

/* -- */

struct integer_node { int val; };
struct float_node { float val; };
struct bool_node { int val; };

/* -- */

struct declaration_node {
	struct node *id;
};

struct block_node {
	struct node *cmd;
	struct node *next;
};

/* -- */

struct if_node {
	struct node *cond;
	struct node *succ_cmd;
	struct node *fail_cmd;
};

struct while_node {
	struct node *cond;
	struct node *cmd;
};

struct arithm_node {
	struct node *lval;
	struct node *expr;
};

struct print_node {
	struct node *value;
};

/* -- */

struct expr_node {
	int size;
	struct node *v1;
	int op;
	struct node *v2;
};

struct value_node {
	enum node_type type;
	union {
		struct id_node *id_n;
		struct integer_node *int_n;
		struct float_node *float_n;
		struct bool_node *bool_n;
	} data;
};

/* -- */

struct node {
	enum node_type type;
	union {
		struct id_node id_n;
		struct integer_node int_n;
		struct float_node float_n;
		struct bool_node bool_n;

		struct arithm_node arithm_n;
		struct if_node if_n;
		struct while_node while_n;
		struct print_node print_n;
		struct declaration_node decl_n;
		struct block_node block_n;

		struct expr_node expr_n;
		struct value_node value_n;
	} data;
};

#endif
