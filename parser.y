%{
#include <string.h>
#include <stdlib.h>
#include "cokl.h"

void free_node(struct node *n);
int ex(struct node *cmd);
int yylex(void);
void yyerror(char *s);

struct node *frameup_decl(struct node *id);
struct node *frameup_if(struct node *cond, struct node *succ,
	struct node *fail);
struct node *frameup_while(struct node *cond, struct node *cmd);
struct node *frameup_print(struct node *value);
struct node *frameup_cond(struct node *e1, int op, struct node *e2);
struct node *frameup_cmd_list(struct node *n1, struct node *n2);

struct node *frameup_arithm(struct node *lval, struct node *expr);
struct node *frameup_expr_3(struct node *e1, int op, struct node *e2);
struct node *frameup_expr_1(struct node *e1);
struct node *frameup_id(char *name);
struct node *frameup_int(int value);
struct node *frameup_float(double value);
struct node *frameup_bool(int value);

%}

%union {
	int i_val;
	char *s_val;
	int b_val;
	double f_val;
	struct node *n_val;
}

%token PROGRAM IF ELSE WHILE RETURN PRINT VAR
%token <i_val> EQ LE GE NE
%token <i_val> INTEGER /* real value */
%token <f_val> FLOAT /* real value */
%token <b_val> BOOL /* real value */
%token <s_val> ID /* real value */

%type <n_val> block declaration cmds cmd arithm if while
%type <n_val> print expr value
%type <i_val> oper '+' '-' '*' '/' '<' '>'

%%

cokl:
  program                      { exit(0); }
  ;

program:
  program cmd                  { ex($2); free_node($2); }
  |
  ;

cmd:
  arithm ';'                   { $$ = $1; }
  | if                         { $$ = $1; }
  | while                      { $$ = $1; }
  | print ';'                  { $$ = $1; }
  | declaration ';'            { $$ = $1; }
  | block                      { $$ = $1; }
  ;

block:
  '{' cmds '}'                 { $$ = $2; }
  ;

cmds:
  cmd cmds                     { $$ = frameup_cmd_list($1, $2); }
  |                            { $$ = NULL; }
  ;

arithm:
  ID '=' expr                  { $$ = frameup_arithm(frameup_id($1), $3); }
  ;

if:
  IF expr cmd ELSE cmd         { $$ = frameup_if($2, $3, $5); }
  ;

while:
  WHILE expr cmd               { $$ = frameup_while($2, $3); }
  ;

print:
  PRINT value                  { $$ = frameup_print($2); }
  ;

declaration:
  VAR ID                       { $$ = frameup_decl(frameup_id($2)); }
  ;

expr:
  value oper value             { $$ = frameup_expr_3($1, $2, $3); }
  | value                      { $$ = frameup_expr_1($1); }
  ;

oper:
  EQ                           { $$ = EQ; }
  | '<'                        { $$ = '<'; }
  | '>'                        { $$ = '>'; }
  | LE                         { $$ = LE; }
  | GE                         { $$ = GE; }
  | NE                         { $$ = NE; }
  | '+'                        { $$ = '+'; }
  | '-'                        { $$ = '-'; }
  | '*'                        { $$ = '*'; }
  | '/'                        { $$ = '/'; }
  ;

value:
  ID                           { $$ = frameup_id(strdup($1)); }
  | INTEGER                    { $$ = frameup_int($1); }
  | FLOAT                      { $$ = frameup_float($1); }
  | BOOL                       { $$ = frameup_bool($1); }
  ;

%%

#define SIZEOF_NODE ((char *)&n->data.id_n - (char *)n)

struct node *frameup_decl(struct node *id)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct declaration_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_declaration;
	n->data.decl_n.id = id;
	return n;
}

struct node *frameup_if(struct node *cond, struct node *succ,
	struct node *fail)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct if_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_if;
	n->data.if_n.cond = cond;
	n->data.if_n.succ_cmd = succ;
	n->data.if_n.fail_cmd = fail;
	return n;
}

struct node *frameup_while(struct node *cond, struct node *cmd)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct while_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_while;
	n->data.while_n.cond = cond;
	n->data.while_n.cmd = cmd;
	return n;
}

struct node *frameup_print(struct node *value)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct print_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_print;
	n->data.print_n.value = value;
	return n;
}

struct node *frameup_expr_3(struct node *e1, int op, struct node *e2)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct expr_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_expr;
	n->data.expr_n.size = 3;
	n->data.expr_n.v1 = e1;
	n->data.expr_n.op = op;
	n->data.expr_n.v2 = e2;
	return n;
}

struct node *frameup_expr_1(struct node *e1)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct expr_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_expr;
	n->data.expr_n.size = 1;
	n->data.expr_n.v1 = e1;
	n->data.expr_n.op = -1;
	n->data.expr_n.v2 = NULL;
	return n;
}

struct node *frameup_cmd_list(struct node *n1, struct node *n2)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct block_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_block;
	n->data.block_n.cmd = n1;
	n->data.block_n.next = n2;
	return n;
}

struct node *frameup_arithm(struct node *lval, struct node *expr)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct arithm_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_arithm;
	n->data.arithm_n.lval = lval;
	n->data.arithm_n.expr = expr;
	return n;
}

struct node *frameup_id(char *name)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct id_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_id;
	n->data.id_n.name = name;
	return n;
}

struct node *frameup_int(int value)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct integer_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_integer;
	n->data.int_n.val = value;
	return n;
}

struct node *frameup_float(double value)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct float_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_float;
	n->data.float_n.val = value;
	return n;
}

struct node *frameup_bool(int value)
{
	struct node *n;
	size_t node_size = SIZEOF_NODE + sizeof(struct bool_node);
	if ((n = malloc(node_size)) == NULL)
		yyerror("out of memory");
	n->type = t_bool;
	n->data.bool_n.val = value;
	return n;
}

void free_node(struct node *n)
{
	if (!n) return;
	switch (n->type) {
	case t_declaration:
		free_node(n->data.decl_n.id);
		break;
	case t_if:
		free_node(n->data.if_n.cond);
		free_node(n->data.if_n.succ_cmd);
		free_node(n->data.if_n.fail_cmd);
		break;
	case t_while:
		free_node(n->data.while_n.cond);
		free_node(n->data.while_n.cmd);
		break;
	case t_print:
		free_node(n->data.print_n.value);
		break;
	case t_expr:
		free_node(n->data.expr_n.v1);
		free_node(n->data.expr_n.v2);
		break;
	case t_block:
		free_node(n->data.block_n.cmd);
		free_node(n->data.block_n.next);
		break;
	case t_arithm:
		free_node(n->data.arithm_n.lval);
		free_node(n->data.arithm_n.expr);
		break;
	case t_id:
		free(n->data.id_n.name);
		break;
	case t_integer:
	case t_float:
	case t_bool:
		/* nothing to free */
		break;
	default:
		yyerror("there is bug in free_node()");
		exit(1);
	}
	free(n);
}
