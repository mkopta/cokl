LEX = flex
YACC = yacc
CFLAGS = -std=c99 -pedantic -Wall

all: cokl

cokl: cokl.o parser.o lexan.o ex.o
	$(CC) $(CFLAGS) $(CLIBS) -o cokl cokl.o parser.o lexan.o ex.o

cokl.o: cokl.c
	$(CC) $(CFLAGS) $(CLIBS) -c -o cokl.o cokl.c

ex.o: ex.c
	$(CC) $(CFLAGS) $(CLIBS) -c -o ex.o ex.c

lexan.c: lexan.l
	$(LEX) -o lexan.c lexan.l

parser.c: parser.y
	$(YACC) -d -o parser.c parser.y

parser.o: parser.c
	$(CC) -c -o parser.o parser.c

lexan.o: lexan.c
	$(CC) -c -o lexan.o lexan.c

clean:
	rm -f parser.h parser.c lexan.c
	rm -f cokl.o parser.o lexan.o ex.o cokl
