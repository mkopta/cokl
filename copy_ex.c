#include <stdio.h>
#include <stdlib.h>
#include "cokl.h"
#include "parser.h"

int indent = 0;

void print_block(struct block_node *);
void print_node(struct node *);

void print_indent(void)
{
	int i;
	for (i = 0; i < indent; i++)
		printf("  ");
}

void print_id(struct id_node *id)
{
	printf("%s", id->name);
}

void print_bool(struct bool_node *b)
{
	printf(b->val ? "true" : "false");
}

void print_integer(struct integer_node *i)
{
	printf("%d", i->val);
}

void print_float(struct float_node *f)
{
	printf("%.3f", f->val);
}

void print_expr(struct expr_node *e)
{
	print_node(e->v1);
	if (e->size == 1) return;
	putchar(' ');
	switch (e->op) {
	case '+': putchar('+'); break;
	case '-': putchar('-'); break;
	case '*': putchar('*'); break;
	case '/': putchar('/'); break;
	case '<': putchar('<'); break;
	case '>': putchar('>'); break;
	case EQ: printf("=="); break;
	case LE: printf("<="); break;
	case GE: printf(">="); break;
	case NE: printf("!="); break;
	default:
		 printf("error. unknown operator.\n");
		 exit(1);
	}
	putchar(' ');
	print_node(e->v2);
}

void print_arithm(struct arithm_node *a)
{
	print_indent();
	print_node(a->lval);
	printf(" = ");
	print_node(a->expr);
	printf(";\n");
}

void print_if(struct if_node *n)
{
	print_indent();
	printf("if ");
	print_node(n->cond);
	printf(" {\n");
	print_node(n->succ_block);
	print_indent();
	printf("} else {");
	print_node(n->fail_block);
	print_indent();
	printf("}\n");
}

void print_while(struct while_node *w)
{
	print_indent();
	printf("while ");
	print_node(w->cond);
	printf(" {\n");
	print_node(w->block);
	print_indent();
	printf("}\n");
}

void print_return(struct return_node *r)
{
	print_indent();
	printf("return ");
	print_node(r->value);
	printf(";\n");
}

void print_print(struct print_node *p)
{
	print_indent();
	printf("print ");
	print_node(p->value);
	printf(";\n");
}

void print_block(struct block_node *cmd_list)
{
	indent++;
	while (1) {
		if (cmd_list->cmd)
			print_node(cmd_list->cmd);
		if (cmd_list->next != NULL)
			cmd_list = &cmd_list->next->data.block_n;
		else
			break;
	}
	indent--;
}

void print_declaration(struct declaration_node *decl)
{
	switch (decl->type) {
	case BOOL_T: printf("bool "); break;
	case FLOAT_T: printf("float "); break;
	case INT_T: printf("int "); break;
	default: printf("error\n"); exit(1);
	}
	print_node(decl->id);
	printf(";\n");
}

void print_declarations(struct declarations_node *decl_list)
{
	while(1) {
		if (decl_list->declaration)
			print_node(decl_list->declaration);
		if (decl_list->next != NULL)
			decl_list = &decl_list->next->data.decls_n;
		else
			break;
	}
	putchar('\n');
}

void print_value(struct value_node *n)
{
	switch(n->type) {
	case t_id: print_id(n->data.id_n); break;
	case t_integer: print_integer(n->data.int_n); break;
	case t_float: print_float(n->data.float_n); break;
	case t_bool: print_bool(n->data.bool_n); break;
	default:
	     printf("error. unknown value.\n");
	     exit(1);
	}
}

void print_node(struct node *n)
{
	if (!n) return;
	switch (n->type) {
	case t_if: print_if(&n->data.if_n); break;
	case t_while: print_while(&n->data.while_n); break;
	case t_return: print_return(&n->data.return_n); break;
	case t_print: print_print(&n->data.print_n); break;
	case t_arithm: print_arithm(&n->data.arithm_n); break;
	case t_integer: print_integer(&n->data.int_n); break;
	case t_float: print_float(&n->data.float_n); break;
	case t_bool: print_bool(&n->data.bool_n); break;
	case t_declarations: print_declarations(&n->data.decls_n); break;
	case t_block: print_block(&n->data.block_n); break;
	case t_id: print_id(&n->data.id_n); break;
	case t_expr: print_expr(&n->data.expr_n); break;
	case t_declaration: print_declaration(&n->data.decl_n); break;
	case t_value: print_value(&n->data.value_n); break;
	default:
	      printf("error. unknown command.\n");
	      exit(1);
	}
}

int ex(char *prgname, struct node *vars, struct node *body)
{
	printf("program %s;\n\n", prgname);
	print_node(vars);
	printf("{\n");
	print_node(body);
	printf("}\n");
	return 0;
}
