#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cokl.h"
#include "parser.h"
#include "tree.h"

char *strdup(char *);

int ex(struct node *cmd);
void ex_block(struct block_node *);

struct collection {
	int i;
	double f;
	int b; /* bool */
	enum node_type type;
};

struct var_node {
	char *var_name;
	struct collection var_value;
	RB_ENTRY(var_node) entry;
};

int varcmp(struct var_node *v1, struct var_node *v2)
{
	return strcmp(v1->var_name, v2->var_name);
}

RB_HEAD(vartree, var_node) head = RB_INITIALIZER(&head);
RB_GENERATE(vartree, var_node, entry, varcmp) 
struct collection new_collection(void)
{
	struct collection c;
	c.i = 0;
	c.f = 0.0;
	c.b = 0;
	c.type = t_undef;
	return c;
}

struct collection *variable_value(struct id_node *id)
{
	struct var_node find, *res;
	if (!id) return NULL;
	find.var_name = id->name;
	res = RB_FIND(vartree, &head, &find);
	if (!res) {
		return NULL;
	} else {
		return &res->var_value;
	}
}

struct collection ex_value(struct node *v)
{
	struct collection c = new_collection(), *varval;
	switch (v->type) {
	case t_integer:
		c.i = v->data.int_n.val;
		c.type = t_integer;
		break;
	case t_float:
		c.f = v->data.float_n.val;
		c.type = t_float;
		break;
	case t_bool:
		c.b = v->data.bool_n.val;
		c.type = t_bool;
		break;
	case t_id:
		varval = variable_value(&v->data.id_n);
		if (varval == NULL || varval->type == t_undef) {
			printf("warning: variable '%s' not defined.\n",
				v->data.id_n.name);
		} else {
			c.i = varval->i;
			c.b = varval->b;
			c.f = varval->f;
			c.type = varval->type;
		}
		break;
	default:
		printf("error. Not a value.\n");
		exit(1);
	}
	return c;
}

void type_unification(struct collection *c1, struct collection *c2)
{
	if (c1->type == t_integer) {
		if (c2->type == t_float) {
			c1->f = (double) c1->i;
			c1->i = 0;
			c1->type = t_float;
		} else if (c2->type == t_bool) {
			c2->i = c2->b;
			c2->b = 0;
			c2->type = t_integer;
		} else if (c2->type != t_integer) {
			c2->type = c1->type;
		}
	} else if (c1->type == t_float) {
		if (c2->type == t_integer) {
			c2->f = (double) c2->i;
			c2->i = 0;
			c2->type = t_float;
		} else if (c2->type == t_bool) {
			c2->f = (double) c2->b;
			c2->b = 0;
			c2->type = t_bool;
		} else if (c2->type != t_float) {
			c2->type = c1->type;
		}
	} else if (c1->type == t_bool) {
		if (c2->type == t_integer) {
			c1->i = c1->b;
			c1->b = 0;
			c1->type = t_integer;
		} else if (c2->type == t_float) {
			c1->f = (double) c1->b;
			c1->b = 0;
			c1->type = t_float;
		} else if (c2->type != t_bool) {
			c2->type = c1->type;
		}
	} else {
		c1->type = c2->type;
	}
	if (c1->type == t_undef)
		c1->type = c2->type = t_integer;
}

struct collection ex_expr(struct expr_node *e)
{
	struct collection c1 = ex_value(e->v1), c2, r;

	if (e->size == 1)
		return c1;

	c2 = ex_value(e->v2);

	type_unification(&c1, &c2);

	switch (e->op) {
	case '+':
		if (c1.type == t_integer) {
			r.type = t_integer;
			r.i = c1.i + c2.i;
		} else if (c1.type == t_float) {
			r.type = t_float;
			r.f = c1.f + c2.f;
		} else if (c1.type == t_bool) {
			r.type = t_bool;
			r.b = c1.b || c2.b;
		}
		break;
	case '-':
		if (c1.type == t_integer) {
			r.type = t_integer;
			r.i = c1.i - c2.i;
		} else if (c1.type == t_float) {
			r.type = t_float;
			r.f = c1.f - c2.f;
		} else if (c1.type == t_bool) {
			r.type = t_bool;
			r.b = ! (c1.b || c2.b);
		}
		break;
	case '*':
		if (c1.type == t_integer) {
			r.type = t_integer;
			r.i = c1.i * c2.i;
		} else if (c1.type == t_float) {
			r.type = t_float;
			r.f = c1.f * c2.f;
		} else if (c1.type == t_bool) {
			r.type = t_bool;
			r.b = c1.b && c2.b;
		}
		break;
	case '/':
		/* TODO FIXME zero division */
		if (c1.type == t_integer) {
			r.type = t_integer;
			r.i = c1.i / c2.i;
		} else if (c1.type == t_float) {
			r.type = t_float;
			r.f = c1.f / c2.f;
		} else if (c1.type == t_bool) {
			r.type = t_bool;
			r.b = ! (c1.b && c2.b);
		}
		break;
	case '<':
		r.type = t_bool;
		if      (c1.type == t_integer) r.b = c1.i < c2.i;
		else if (c1.type == t_float)   r.b = c1.f < c2.f;
		else if (c1.type == t_bool)    r.b = c1.b < c2.b;
		break;
	case '>':
		r.type = t_bool;
		if      (c1.type == t_integer) r.b = c1.i > c2.i;
		else if (c1.type == t_float)   r.b = c1.f > c2.f;
		else if (c1.type == t_bool)    r.b = c1.b > c2.b;
		break;
	case EQ:
		r.type = t_bool;
		if      (c1.type == t_integer) r.b = c1.i == c2.i;
		else if (c1.type == t_float)   r.b = c1.f == c2.f;
		else if (c1.type == t_bool)    r.b = c1.b == c2.b;
		break;
	case LE:
		r.type = t_bool;
		if      (c1.type == t_integer) r.b = c1.i <= c2.i;
		else if (c1.type == t_float)   r.b = c1.f <= c2.f;
		else if (c1.type == t_bool)    r.b = c1.b <= c2.b;
		break;
	case GE:
		r.type = t_bool;
		if      (c1.type == t_integer) r.b = c1.i >= c2.i;
		else if (c1.type == t_float)   r.b = c1.f >= c2.f;
		else if (c1.type == t_bool)    r.b = c1.b >= c2.b;
		break;
	case NE:
		r.type = t_bool;
		if      (c1.type == t_integer) r.b = c1.i != c2.i;
		else if (c1.type == t_float)   r.b = c1.f != c2.f;
		else if (c1.type == t_bool)    r.b = c1.b != c2.b;
		break;
	default:
		printf("error. unknown operator.\n");
		exit(1);
	}

	return r;
}

void ex_arithm(struct arithm_node *a)
{
	struct collection expr = ex_expr(&a->expr->data.expr_n), *varval;
	varval = variable_value(&a->lval->data.id_n);
	if (!varval) {
		printf("error. variable '%s' not defined.\n",
			a->lval->data.id_n.name);
	} else {
		varval->type = expr.type;
		varval->i = expr.i;
		varval->b = expr.b;
		varval->f = expr.f;
	}
}

int ex_cond(struct expr_node *e)
{
	int cond_val = 0;
	struct collection cond = ex_expr(e);
	switch (cond.type) {
	case t_integer:
		cond_val = cond.i;
		break;
	case t_bool:
		cond_val = cond.b;
		break;
	case t_float:
		cond_val = cond.f;
		break;
	default:
		printf("error\n");
		exit(1);
	}
	return cond_val;
}

void ex_if(struct if_node *n)
{
	if (ex_cond(&n->cond->data.expr_n)) {
		ex(n->succ_cmd);
	} else {
		ex(n->fail_cmd);
	}
}

void ex_while(struct while_node *w)
{
	while (ex_cond(&w->cond->data.expr_n))
		ex(w->cmd);
}

void ex_print(struct print_node *p)
{
	struct collection val = ex_value(p->value);
	switch (val.type) {
	case t_integer:
		printf("%d\n", val.i);
		break;
	case t_bool:
		printf("%s\n", val.b ? "true" : "false");
		break;
	case t_float:
		printf("%.3f\n", val.f);
		break;
	default:
		printf("error\n");
		exit(1);
	}
}

void ex_block(struct block_node *cmd_list)
{
	while (1) {
		if (cmd_list->cmd)
			ex(cmd_list->cmd);
		if (cmd_list->next != NULL)
			cmd_list = &cmd_list->next->data.block_n;
		else
			break;
	}
}

void ex_declaration(struct declaration_node *decl)
{
	char *var_name = strdup(decl->id->data.id_n.name);
	struct collection var_value = new_collection();
	struct var_node *var;

	if ((var = malloc(sizeof(struct var_node))) == NULL) {
		printf("out of memory.\n");
		exit(1);
	}

	var->var_name = var_name;
	var->var_value = var_value;

	RB_INSERT(vartree, &head, var);
}

int ex(struct node *n)
{
	if (!n) return 1;
	switch (n->type) {
	case t_if:
		ex_if(&n->data.if_n);
		break;
	case t_while:
		ex_while(&n->data.while_n);
		break;
	case t_print:
		ex_print(&n->data.print_n);
		break;
	case t_arithm:
		ex_arithm(&n->data.arithm_n);
		break;
	case t_block:
		ex_block(&n->data.block_n);
		break;
	case t_declaration:
		ex_declaration(&n->data.decl_n);
		break;
	default:
		printf("error. unknown command (%d).\n", n->type);
		exit(1);
	}
	return 0;
}
