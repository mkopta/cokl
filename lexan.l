%{
#include "parser.h"
#include "cokl.h"
%}

%%

"program"                   return PROGRAM;
"if"                        return IF;
"else"                      return ELSE;
"while"                     return WHILE;
"return"                    return RETURN;
"print"                     return PRINT;
"var"                       return VAR;

"=="                        return EQ;
"<="                        return LE;
">="                        return GE;
"!="                        return NE;

"true"                     { yylval.b_val = 1; return BOOL; }
"false"                    { yylval.b_val = 0; return BOOL; }
[a-z][-_a-z0-9]*           { yylval.s_val = strdup(yytext); return ID; }
[0-9]*[.][0-9][0-9]*       { yylval.f_val = atof(yytext); return FLOAT; }
[0-9][0-9]*                { yylval.i_val = atoi(yytext); return INTEGER; }

[-{};+*/<>=]               return *yytext;

[ \t\n]+                   ;


%%

int yywrap(void)
{
	return 1;
}

void yyerror(char *s)
{
	fprintf(stderr, "%s\n", s);
}
